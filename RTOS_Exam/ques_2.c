#include<stdio.h>
#include<unistd.h>
#include<sys/mman.h>
#include <rtdk.h>
#include <signal.h>
#include<native/task.h>
#include<native/sem.h>
#include <native/pipe.h>
#include<string.h>

#define NTASKS 4

RT_PIPE p;

void sigint_handler(int sig)
{
}

void pipe_func(void* param)
{
    long num = (long)param;
    int i,ret;
    char buf[32];
    RT_TASK_INFO info;
    rt_task_inquire(NULL ,&info);

    sprintf(buf,"%s",info.name);
    sprintf(buf,"%d",info.bprio);

    ret =rt_pipe_write(&p,&buf,strlen(buf)+1,P_NORMAL);
     if(ret<0)
        {
            rt_printf("rt_pipe_write() failed.\n");
            return;
        }
        rt_task_sleep(1000000000);
    rt_task_delete(NULL);
}

int main(void)
{
    int i , ret;
    struct sigaction sa;
    RT_TASK t1;

    rt_print_auto_init(1);
    rt_printf("Question 2 Program.\n");


    ret = mlockall(MCL_CURRENT | MCL_FUTURE);
    if(ret!=0)
    {
        rt_printf("mlockall() failed.\n");
        _exit(1);
    }

    memset(&sa , 0 , sizeof(sa));
    sa.sa_handler = sigint_handler;
    ret = sigaction(SIGINT , &sa , NULL);
    if(ret!=0)
        {
            rt_printf("sigaction() failed.\n");
            _exit(1);
        }

    ret = rt_pipe_create(&p,"NULL",2,1000);
    if(ret!=0)
        {
            rt_printf("rt_pipe_create() failed.\n");
            _exit(1);
        }
    
        ret = rt_task_create(&t1,"name and prio",0,50,T_CPU(0));
        if(ret!=0)
        {
            rt_printf("rt_task_create() failed.\n");
            _exit(1);
        }
        ret = rt_task_start(&t1, pipe_func, NULL);
        if(ret!=0)
        {
            rt_printf("task_start() failed.\n");
            _exit(1);
        }
   
    pause();

    return 0;
}