#include<stdio.h>
#include<unistd.h>
#include<sys/mman.h>
#include <rtdk.h>
#include <signal.h>
#include<native/task.h>
#include<native/sem.h>



#define NTASKS 4

RT_SEM sem;

void sigint_handler(int sig)
{
}

void table_func(void* param)
{
    long num = (long)param;
    int i;
    RT_TASK_INFO info;
    rt_task_inquire(NULL ,&info);
    rt_sem_p(&sem, TM_INFINITE);

    for(i=1;i<=10;i++)
    {
        rt_printf("Task Running %s (prio = %d) Table    : %ld * %ld = %ld.\n",info.name , info.bprio , num , i , num*i);
        if(i % 3 == 0)
		    rt_task_yield();
    }
    rt_task_delete(NULL);
}

int main(void)
{
    RT_TASK task[NTASKS];
    int i , ret;
    char name[8];
    struct sigaction sa;


    rt_print_auto_init(1);
    rt_printf("Question 1 Program.\n");


    ret = mlockall(MCL_CURRENT | MCL_FUTURE);
    if(ret!=0)
    {
        rt_printf("mlockall() failed.\n");
        _exit(1);
    }

    memset(&sa , 0 , sizeof(sa));
    sa.sa_handler = sigint_handler;
    ret = sigaction(SIGINT , &sa , NULL);
    if(ret!=0)
        {
            rt_printf("sigaction() failed.\n");
            _exit(1);
        }

    ret = rt_sem_create(&sem,"sem_table,",0,S_FIFO);
    if(ret!=0)
       {
           rt_printf("rt_sem_create() failed.\n");
           _exit(1);
       }
    for(i=1;i<=NTASKS;i++)
    {
        sprintf(name, "print_table of %d", i);
        ret = rt_task_create(&task[i],name,0,50,T_CPU(0));
        if(ret!=0)
        {
            rt_printf("rt_task_create() failed.\n");
            _exit(1);
        }
        ret = rt_task_start(&task[i], table_func, (void *)i);
        if(ret!=0)
        {
            rt_printf("task_start() failed.\n");
            _exit(1);
        }
   }
   //rt_printf("press ctrl+c to see blocked process.\n");
   //pause();

   rt_sem_broadcast(&sem);
   pause();

    rt_sem_delete(&sem);

    return 0;
}